package test.MyTest.configuration;


import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

/**
 * This is DBConfiguration.
 *
 * @author Aliaksandra
 */
@Configuration
@ConfigurationProperties("spring.datasource")
public class DBConfiguration {

    private String driverClassName;
    private String url;
    private String username;
    private String password;

    public String getDriverClassName() {
        return driverClassName;
    }

    public void setDriverClassName(String driverClassName) {
        this.driverClassName = driverClassName;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * This is the method that returns connection to MySQL base.
     *
     * @return connection
     */
    @Profile("dev")
    @Bean
    public String devDatabaseConnection() {
        System.out.println("DB connection for DEV - MySQL");
        System.out.println(driverClassName);
        System.out.println(url);
        return "DB connection for DEV - MySQL";
    }

    /**
     * This is the method that returns connection to H2 base.
     *
     * @return connection
     */
    @Profile("test")
    @Bean
    public String testDatabaseConnection() {
        System.out.println("DB Connection for TEST - H2");
        System.out.println(driverClassName);
        System.out.println(url);
        return "DB Connection to TEST - H2";
    }
}