package test.MyTest.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;
import test.MyTest.models.Resume;

import java.util.List;

@Component
public interface ResumeRepository extends JpaRepository<Resume, Long> {

}
