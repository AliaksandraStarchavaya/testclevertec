package test.MyTest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import test.MyTest.dao.interfaces.ResumeService;
import test.MyTest.models.Resume;

import java.util.List;

/**
 * This is Controller.
 *
 * @author Aliaksandra
 */
@RestController
@RequestMapping("resume")
public class MyController {
    /**
     * This value resumeService
     */
    private final ResumeService resumeService;

    /**
     * This is the method that returns all resumes.
     *
     * @param resumeService takes resumeService
     */
    @Autowired
    public MyController(ResumeService resumeService) {
        this.resumeService = resumeService;
    }

    /**
     * This is the method that returns all resumes.
     *
     * @return all resumes
     */
    @GetMapping
    public List<Resume> getResumes() {
        return resumeService.getResumes();
    }

    /**
     * This is the method that returns one resume by id.
     *
     * @param id id
     * @return resume
     */
    @GetMapping("{id}")
    public Resume getOneRecume(@PathVariable long id) {
        return resumeService.getOneResume(id);
    }

    /**
     * This is the method that create resume.
     *
     * @param resume resume
     * @return resumeFromeDB
     */
    @PostMapping
    public Resume createResume(@RequestBody Resume resume) {
        /*
      fetch('http://localhost:8080/resume', {
    method: 'post',
    body: JSON.stringify({surname:'Koval' ,name:'Dima'}),
    headers: {
        'content-type': 'application/json'
            }
          })
         */
        //  resume.setId(counter++);
        return resumeService.createResume(resume);
    }

    /**
     * This is the method that delete resume by id.
     *
     * @param id id resume
     */
    @DeleteMapping("{id}")
    public void deleteResume(@PathVariable long id) {
        resumeService.deleteResume(id);
        //fetch('/resume/4', { method: 'DELETE' }).then(result => console.log(result))
    }

    /**
     * This is the method that update resume.
     *
     * @param resume resume to replace
     * @param id     id resume to replace
     * @return resumeFromeDB
     */
    @PutMapping("{id}")
    public Resume update(@PathVariable long id, @RequestBody Resume resume) {
/*
               fetch('http://localhost:8080/resume/3',{
               method: 'PUT',
               headers: { 'Content-Type': 'application/json' },
               body: JSON.stringify({id:10, surname:'Ivanov' ,name:'Ivan'})}
               ).then(result => result.json().then(console.log));
*/
        return resumeService.updateResume(id, resume);
    }

    @RequestMapping("/search")
    public List<Resume> search(String q) {
        System.out.println("3333333" + q);
        List<Resume> searchResults = null;
        try {
            searchResults = resumeService.search(q);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return searchResults;
        //search?q=Koval
    }


}
