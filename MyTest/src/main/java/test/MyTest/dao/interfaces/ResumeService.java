package test.MyTest.dao.interfaces;

import test.MyTest.models.Resume;
import org.springframework.data.domain.Pageable;
import java.util.List;

public interface ResumeService {
    List<Resume> getResumes();
    Resume getOneResume(long id);
    Resume createResume(Resume resume);
    void deleteResume(long id);
    Resume updateResume(long id,Resume resume);
    List<Resume> search(String text);
}
