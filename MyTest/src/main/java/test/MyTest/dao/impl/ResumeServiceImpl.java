package test.MyTest.dao.impl;

import org.hibernate.search.jpa.FullTextEntityManager;
import org.hibernate.search.query.dsl.QueryBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import test.MyTest.dao.interfaces.ResumeService;
import test.MyTest.exceptions.NotFoundException;
import test.MyTest.models.Resume;
import test.MyTest.repo.ResumeRepository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Service
@Transactional
public class ResumeServiceImpl implements ResumeService {
    @Autowired
    private ResumeRepository repository;
    @PersistenceContext
    private EntityManager entityManager;

    public ResumeServiceImpl() {
    }

    public ResumeServiceImpl(ResumeRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<Resume> search(String text) {

        FullTextEntityManager fullTextEntityManager =
                org.hibernate.search.jpa.Search.getFullTextEntityManager(entityManager);
        QueryBuilder qb = fullTextEntityManager.getSearchFactory()
                .buildQueryBuilder().forEntity(Resume.class).get();
        javax.persistence.Query jpaQuery =
                fullTextEntityManager.createFullTextQuery(createQuery(qb, text), Resume.class);
        @SuppressWarnings("unchecked")
        List<Resume> result = jpaQuery.getResultList();
        return result;
    }

    private org.apache.lucene.search.Query createQuery(QueryBuilder qb, String keyword) {
        return qb
                .keyword()
                .onField("name")
                .matching(keyword)
                .createQuery();
    }


    @Override
    public List<Resume> getResumes() {
        return repository.findAll();
    }

    private Resume getResume(@PathVariable long id) {
        return repository.findAll().stream()
                .filter(resume -> resume.getId() == id)
                .findFirst()
                .orElseThrow(NotFoundException::new);
    }

    @Override
    public Resume getOneResume(long id) {
        return getResume(id);
    }

    @Override
    public Resume createResume(Resume resume) {
        repository.save(resume);
        return resume;
    }

    @Override
    public void deleteResume(long id) {
        repository.delete(getResume(id));
    }

    @Override
    public Resume updateResume(long id, Resume resume) {
        Resume resumeFromDb = getResume(id);
        resumeFromDb.setName(resume.getName());
        resumeFromDb.setSurname(resume.getSurname());
        resumeFromDb.setId(id);
        return resumeFromDb;
    }
}
