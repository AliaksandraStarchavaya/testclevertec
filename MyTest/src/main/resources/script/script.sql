DROP DATABASE IF EXISTS resumeDB;
CREATE DATABASE resumeDB;

CREATE TABLE resumeDB.resume (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `surname` varchar(45) DEFAULT NULL,
  `name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
);
