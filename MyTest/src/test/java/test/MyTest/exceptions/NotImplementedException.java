package test.MyTest.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

public class NotImplementedException extends RuntimeException {
}
