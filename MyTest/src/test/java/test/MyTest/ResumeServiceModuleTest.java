package test.MyTest;

import org.junit.jupiter.api.Test;
import test.MyTest.dao.impl.ResumeServiceImpl;
import test.MyTest.dao.interfaces.ResumeService;
import test.MyTest.mock.MockResumeRepo;
import test.MyTest.models.Resume;

import static org.junit.jupiter.api.Assertions.*;

public class ResumeServiceModuleTest {
    private final ResumeService mockResumeService = new ResumeServiceImpl(new MockResumeRepo());

    @Test
    public void createResume_valid_sucsess() {
        //arrange
        Resume resumeModel = new Resume("Anna", "Valera");
        //act
        Resume resumeFromDB = mockResumeService.createResume(resumeModel);
        //assert
        assertEquals(resumeFromDB, resumeModel);
        assertTrue(resumeFromDB.getId() !=0);
    }

    @Test
    public void findAllResume_valid_sucsess() {
        //arrange
        Resume resumeModel1 = new Resume("Anna", "Kovaleva");
        Resume resumeModel2 = new Resume("Valera", "Kovalev");
        //act
        Resume resumeFromDB1 = mockResumeService.createResume(resumeModel1);
        Resume resumeFromDB2 = mockResumeService.createResume(resumeModel2);
        //assert
        assertEquals(resumeFromDB1, resumeModel1);
        assertEquals(resumeFromDB2, resumeModel2);
        assertEquals(mockResumeService.getResumes().size(), 2);
    }

    @Test
    public void deleteResume_valid_sucsess() {
        //arrange
        Resume resumeModel1 = new Resume("Anna", "Kovaleva");
        Resume resumeModel2 = new Resume("Valera", "Kovalev");
        //act
        Resume resumeFromDB1 = mockResumeService.createResume(resumeModel1);
        mockResumeService.createResume(resumeModel2);
        mockResumeService.deleteResume(resumeFromDB1.getId());
        //assert
        assertEquals(mockResumeService.getResumes().size(), 1);
    }

    @Test
    public void updateResume_valid_sucsess() {
        //arrange
        Resume resumeModel1 = new Resume("Anna", "Kovaleva");
        Resume resumeModel2 = new Resume("Valera", "Kovalev");
        //act
        Resume resumeFromDB1 = mockResumeService.createResume(resumeModel1);
        mockResumeService.updateResume(1, resumeModel2);
        //assert
        assertEquals(resumeFromDB1, resumeModel2);
        assertEquals(resumeFromDB1.getId(), 1);
    }

    @Test
    public void getOneResume_valid_sucsess() {
        //arrange
        Resume resumeModel1 = new Resume("Anna", "Kovaleva");
        Resume resumeModel2 = new Resume("Valera", "Kovalev");
        //act
        mockResumeService.createResume(resumeModel1);
        mockResumeService.createResume(resumeModel2);
        Resume resumeFromDB1 = mockResumeService.getOneResume(1);
        Resume resumeFromDB2 = mockResumeService.getOneResume(2);
        //assert
        assertEquals(resumeFromDB1, resumeModel1);
        assertEquals(resumeFromDB1.getId(), 1);
        assertEquals(resumeFromDB2, resumeModel2);
        assertEquals(resumeFromDB2.getId(), 2);
    }
}
