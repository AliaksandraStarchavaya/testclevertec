package test.MyTest;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Scope;
import test.MyTest.dao.impl.ResumeServiceImpl;
import test.MyTest.dao.interfaces.ResumeService;
import test.MyTest.mock.MockResumeRepo;
import test.MyTest.models.Resume;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
public class ResumeServiceIntegrationTest {

    private final ResumeService resumeService;
    @Autowired
    public ResumeServiceIntegrationTest(ResumeService resumeService) {
        this.resumeService = resumeService;
    }

    @Test
    public void createResume_valid_sucsess() {
        //arrange
        Resume resumeModel = new Resume("Anna", "Valera");
        //act
        Resume resumeFromDB = resumeService.createResume(resumeModel);
        //assert
        assertEquals(resumeModel, resumeFromDB);
        assertEquals(resumeService.getResumes().size(), 1);
    }

    @Test
    public void findAllResume_valid_sucsess() {
        //arrange
        Resume resumeModel1 = new Resume("Anna", "Kovaleva");
        Resume resumeModel2 = new Resume("Valera", "Kovalev");
        //act
        int sizeResume=resumeService.getResumes().size();
        //assert
        Resume resumeFromDB1 = resumeService.createResume(resumeModel1);
        Resume resumeFromDB2 = resumeService.createResume(resumeModel2);
        //assert
        assertEquals(resumeModel1, resumeFromDB1);
        assertEquals(resumeModel2, resumeFromDB2);
        assertEquals(resumeService.getResumes().size(), sizeResume+2);
    }

    @Test
    public void deleteResume_valid_sucsess() {
        //arrange
        Resume resumeModel = new Resume("Anna", "Kovaleva");
        //act
        int sizeResume=resumeService.getResumes().size();
        Resume resumeFromDB = resumeService.createResume(resumeModel);
        resumeService.deleteResume(resumeFromDB.getId());
        int sizeResume2=resumeService.getResumes().size();
        //assert
        assertEquals(sizeResume, sizeResume2);
    }

    @Test
    public void updateResume_valid_sucsess() {
        //arrange
        Resume resumeModel = new Resume("Anna", "Kovaleva");
        //act
        resumeService.createResume(resumeModel);
        Resume resumeAfterEdit=resumeService.updateResume(1, resumeModel);
        //assert
        assertEquals(resumeAfterEdit, resumeModel);
    }

    @Test
    public void getOneResume_valid_sucsess() {
        //arrange
        Resume resumeModel1 = new Resume("Anna", "Kovaleva");
        Resume resumeModel2 = new Resume("Valera", "Kovalev");
        //act
        Resume resume1=resumeService.createResume(resumeModel1);
        Resume resume2=resumeService.createResume(resumeModel2);
        Resume resumeFromDB1 = resumeService.getOneResume(resume1.getId());
        Resume resumeFromDB2 = resumeService.getOneResume(resume2.getId());
        //assert
        assertEquals(resumeFromDB1, resume1);
        assertEquals(resumeFromDB2, resume2);
    }
}
