package test.MyTest.mock;

import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import test.MyTest.exceptions.NotImplementedException;
import test.MyTest.models.Resume;
import test.MyTest.repo.ResumeRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class MockResumeRepo implements ResumeRepository {

    private List<Resume> mockDB = new ArrayList<>();
    private int counter = 1;

    @Override
    public List<Resume> findAll() {
        return mockDB;
    }

    @Override
    public List<Resume> findAll(Sort sort) {
        throw new NotImplementedException();
    }

    @Override
    public Page<Resume> findAll(Pageable pageable) {
        throw new NotImplementedException();
    }

    @Override
    public List<Resume> findAllById(Iterable<Long> longs) {
        throw new NotImplementedException();
    }

    @Override
    public long count() {
        throw new NotImplementedException();
    }

    @Override
    public void deleteById(Long aLong) {
        mockDB.remove(aLong);
    }

    @Override
    public void delete(Resume entity) {
        mockDB.remove(entity);
    }

    @Override
    public void deleteAll(Iterable<? extends Resume> entities) {
        throw new NotImplementedException();
    }

    @Override
    public void deleteAll() {
        throw new NotImplementedException();
    }

    @Override
    public <S extends Resume> S save(S entity) {
        entity.setId(counter++);
        mockDB.add(entity);
        return entity;
    }

    @Override
    public <S extends Resume> List<S> saveAll(Iterable<S> entities) {
        throw new NotImplementedException();
    }

    @Override
    public Optional<Resume> findById(Long aLong) {
        throw new NotImplementedException();
    }

    @Override
    public boolean existsById(Long aLong) {
        throw new NotImplementedException();
    }

    @Override
    public void flush() {
        throw new NotImplementedException();
    }

    @Override
    public <S extends Resume> S saveAndFlush(S entity) {
        throw new NotImplementedException();
    }

    @Override
    public void deleteInBatch(Iterable<Resume> entities) {
        throw new NotImplementedException();
    }

    @Override
    public void deleteAllInBatch() {
        throw new NotImplementedException();
    }

    @Override
    public Resume getOne(Long aLong) {
        throw new NotImplementedException();
    }

    @Override
    public <S extends Resume> Optional<S> findOne(Example<S> example) {
        throw new NotImplementedException();
    }

    @Override
    public <S extends Resume> List<S> findAll(Example<S> example) {
        throw new NotImplementedException();
    }

    @Override
    public <S extends Resume> List<S> findAll(Example<S> example, Sort sort) {
        throw new NotImplementedException();
    }

    @Override
    public <S extends Resume> Page<S> findAll(Example<S> example, Pageable pageable) {
        throw new NotImplementedException();
    }

    @Override
    public <S extends Resume> long count(Example<S> example) {
        throw new NotImplementedException();
    }

    @Override
    public <S extends Resume> boolean exists(Example<S> example) {
        throw new NotImplementedException();
    }
}
